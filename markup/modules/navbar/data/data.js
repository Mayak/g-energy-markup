data = {
	navbar: {
		navigation:[
			{
				title:"О бренде",
				href:"#"
			},
			{
				title:"Продукция",
				href:"#"
			},
			{
				title:"Подбор масла",
				href:"#",
				attr:'data-toggle=modal data-target=#oil-modal'
			},
			{
				title:"Где купить",
				href:"#"
			},
			{
				title:"G-energy service",
				href:"#"
			},
			{
				title:"Новости",
				href:"#"
			}
		]
	}
};